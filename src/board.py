from dataclasses import dataclass
import copy
from typing import List, Union
Piece = List[bool]


board_X, board_Y = (4, 4)

def new_piece(color: bool, height: bool, hollow: bool, shape: bool):
    return [color, height, hollow, shape]

@dataclass
class QuartoBoard():
    pieces: List[List[Piece | None]]

    def __init__(self, orig=None):
        # Plain constructor
        if orig is None:
            # Empty board
            self.pieces = []
            for x in range(board_X):
                self.pieces.append([None] * board_Y)
            pass
        # Copy constructor
        else: 
            self = copy.deepcopy(orig)
            pass

    def print_piece(self, piece:Piece|None):
        if piece is None:
            print("----",end='')
            return
        
        print("".join([str(int(prop)) for prop in piece]), end='')

    def print_board(self):
        for row in self.pieces:
            for piece in row:
                self.print_piece(piece)
                print(" ", end='')
            print() # Newline